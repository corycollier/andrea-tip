var app = angular.module('app', []);

app.controller('AppCtrl', function($scope) {

  $scope.meal = {
    cost : null,

    tip  : '15',

    amt  : function ( ) {
      return parseFloat((this.tip / 100) * this.cost);
    },

    total : function ( ) {
      var cost = parseFloat(this.cost)
        , tip  = this.amt()
      ;

      return cost + tip;
    }
  };

  $scope.reset = function ( ) {
    $scope.meal.cost = 0;
    $scope.meal.tip = '15' ;
  };

});